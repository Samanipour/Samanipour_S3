package com.example.itcenter.samanipour_s3.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.itcenter.samanipour_s3.Models.StudentModel;
import com.example.itcenter.samanipour_s3.R;

import java.util.List;

/**
 * Created by IT CENTER on 11/24/2017.
 */

public class StudentsListAdapter extends BaseAdapter{
    Context mContext;
    List<StudentModel> students;

    public StudentsListAdapter(Context mContext, List<StudentModel> students) {
        this.mContext = mContext;
        this.students = students;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Object getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.students_list_item, viewGroup, false);
        StudentModel thsistudent =students.get(position);


        TextView name=(TextView)rowView.findViewById(R.id.name);
        TextView family=(TextView)rowView.findViewById(R.id.family);
        TextView age=(TextView)rowView.findViewById(R.id.age);

        name.setText(thsistudent.getName());
        family.setText(thsistudent.getFamily());
        age.setText(thsistudent.getAge()+"");

        return rowView;
    }
}

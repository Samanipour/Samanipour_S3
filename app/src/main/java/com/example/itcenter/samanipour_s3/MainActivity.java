package com.example.itcenter.samanipour_s3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView lnk_sabt;
    TextView lnk_list;
    TextView lnk_grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lnk_sabt = (TextView) findViewById(R.id.lnk_sabt);
        lnk_list = (TextView) findViewById(R.id.lnk_list);
        lnk_grid = (TextView) findViewById(R.id.lnk_grid);

        lnk_sabt.setOnClickListener(this);
        lnk_list.setOnClickListener(this);
        lnk_grid.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.lnk_sabt) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.lnk_list) {
            Intent intent = new Intent(MainActivity.this, StudentsListActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.lnk_grid) {
            Intent intent = new Intent(MainActivity.this, StudentGridActivity.class);
            startActivity(intent);

        }
    }
}

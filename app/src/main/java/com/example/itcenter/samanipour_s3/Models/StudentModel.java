package com.example.itcenter.samanipour_s3.Models;

/**
 * Created by IT CENTER on 11/24/2017.
 */

public class StudentModel {
    String name;
    String family;
    int age;

    public StudentModel(String name, String family, int age) {
        this.name = name;
        this.family = family;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

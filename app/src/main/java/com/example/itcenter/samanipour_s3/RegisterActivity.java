package com.example.itcenter.samanipour_s3;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.itcenter.samanipour_s3.Utility.PublicMethods;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    EditText name;
    EditText family;
    EditText age;
    ImageButton btn_save;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        BindLoad();
    }

    private void BindLoad() {
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        age = (EditText) findViewById(R.id.age);
        btn_save = (ImageButton) findViewById(R.id.btn_save);

        btn_save.setOnClickListener(this);

        name.setText(PublicMethods.getShared(mContext,"name",""));
        family.setText(PublicMethods.getShared(mContext,"family",""));
        age.setText(PublicMethods.getShared(mContext,"age",""));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_save) {
            String nameValue = name.getText().toString();
            String familyValue = family.getText().toString();
            String ageValue = age.getText().toString();

            PublicMethods.SetShared(mContext,"Name",nameValue);
            PublicMethods.SetShared(mContext,"Family",familyValue);
            PublicMethods.SetShared(mContext,"Age",ageValue);
            name.setText("");
            family.setText("");
            age.setText("");
            Toast.makeText(mContext, "Saved....OK", Toast.LENGTH_SHORT).show();
        }
    }

}

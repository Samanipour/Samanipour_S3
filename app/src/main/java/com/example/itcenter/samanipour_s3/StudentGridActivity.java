package com.example.itcenter.samanipour_s3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;


import com.example.itcenter.samanipour_s3.Adapters.StudentsListAdapter;
import com.example.itcenter.samanipour_s3.Models.StudentModel;

import java.util.ArrayList;
import java.util.List;

public class StudentGridActivity extends BaseActivity {

    GridView studentslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_grid);
        studentslist = (GridView) findViewById(R.id.student_list);
        StudentModel p1 = new StudentModel("Ali", "Rezaei", 30);
        StudentModel p2 = new StudentModel("Javad", "Alamati", 30);
        StudentModel p3 = new StudentModel("Reza", "Montazeri", 30);
        StudentModel p4 = new StudentModel("Hossein", "Jonoobi", 30);
        StudentModel p5 = new StudentModel("Mohammad", "Motamedi", 30);
        StudentModel p6 = new StudentModel("Mahmood", "Nasi", 30);
        StudentModel p7 = new StudentModel("Morteza", "Mehrani", 30);
        StudentModel p8 = new StudentModel("Navid", "Ghayoori", 30);
        StudentModel p9 = new StudentModel("Shahin", "Sabet", 30);
        StudentModel p10 = new StudentModel("Shahrooz", "Ostadi", 30);
        StudentModel p11 = new StudentModel("Saeed", "Mehri", 30);
        StudentModel p12 = new StudentModel("Gholi", "Keshvari", 30);

        List<StudentModel> students = new ArrayList<StudentModel>();
        students.add(p1);
        students.add(p2);
        students.add(p6);
        students.add(p4);
        students.add(p5);
        students.add(p6);
        students.add(p7);
        students.add(p8);
        students.add(p9);
        students.add(p10);
        students.add(p11);
        students.add(p12);

        StudentsListAdapter adapter = new StudentsListAdapter(mContext, students);
        studentslist.setAdapter(adapter);


    }
}

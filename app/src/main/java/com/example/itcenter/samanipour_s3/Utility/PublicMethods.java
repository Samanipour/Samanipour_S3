package com.example.itcenter.samanipour_s3.Utility;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by IT CENTER on 11/24/2017.
 */

public class PublicMethods {

   public static void SetShared(Context mContext, String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();

    }

    public static String getShared(Context mContext,String key, String value) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, value);
    }
}
